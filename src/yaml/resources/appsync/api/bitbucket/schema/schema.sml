type Repository {
  uuid: String
  full_name: String
  is_private: Boolean
  scm: String
  name: String
  description: String
  created_on: String
  updated_on: String
  size: String
  language: String
  has_issues: Boolean
  has_wiki: Boolean
  fork_policy: String
  project: Project
}

type PaginatedRepositories {
  size: Int
  page: Int
  pagelen: Int
  next: String
  previous: String
  values: [Repository]
}

type PipelineVariable {
  uuid: String
  key: String
  value: String
  secured: Boolean
}

type PaginatedPipelineVariables {
  size: Int
  page: Int
  pagelen: Int
  next: String
  previous: String
  values: [PipelineVariable]
}

type PipelinesConfig {
  variables: PaginatedPipelineVariables
}

type SearchResult {
  empty: String
}

type SearchResultPage {
  size: Int
  page: Int
  pagelen: Int
  next: String
  previous: String
  values: [SearchResult]
}

type SshKey {
  uuid: String
  key: String
  comment: String
  label: String
  created_on: String
  last_used: String
}

type PaginatedSshUserKeys {
  size: Int
  page: Int
  pagelen: Int
  next: String
  previous: String
  values: [SshKey]
}

enum SubjectType {
  user
  repository
  team
}

type WebhookSubscription {
 uuid: String
 url: String
 description: String
 subject_type: SubjectType
 subject: String
 active: Boolean
 created_at: String
 events: [String]
}

type PaginatedWebHookSubscriptions {
  size: Int
  page: Int
  pagelen: Int
  next: String
  previous: String
  values: [WebhookSubscription]
}

type User {
  username: String
  display_name: String
  website: String
  followers: PaginatedUsers
  following: PaginatedUsers
  hooks: PaginatedWebHookSubscriptions
  pipelines_config: PipelinesConfig
  ssh_keys: PaginatedSshUserKeys
  repositories: PaginatedRepositories
  search: SearchResultPage
}

type PaginatedUsers {
  size: Int
  page: Int
  pagelen: Int
  next: String
  previous: String
  values: [User]
}  

enum TeamPermissionType {
  admin
  collaborator
}

type TeamPermission {
  permission: TeamPermissionType
  user: User
  team: Team
}

type PaginatedTeamPermissions {
  size: Int
  page: Int
  pagelen: Int
  next: String
  previous: String
  values: [TeamPermission]
}


type Project {
  uuid: String
  key: String
  name: String
  description: String
  is_private: String
  created_on: String
  updated_on: String
}

type PaginatedProjects {
  size: Int
  page: Int
  pagelen: Int
  next: String
  previous: String
  values: [Project]
}

type Team {
  username: String
  display_name: String
  website: String
  members: [User]
  repositories: PaginatedRepositories
  permissions: PaginatedTeamPermissions
  hooks: PaginatedWebHookSubscriptions
  pipelines_config: PipelinesConfig
  projects: PaginatedProjects
  search: SearchResultPage
}

type PaginatedTeams {
  size: Int
  page: Int
  pagelen: Int
  next: String
  previous: String
  values: [Team]
}

type Api {
  user: User
  teams: PaginatedTeams
}

type Query {
  bitbucket: Api
}

schema {
  query: Query
}
